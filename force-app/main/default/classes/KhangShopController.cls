public with sharing class KhangShopController {
    public KhangShopController() {

    }
     //Get Account Records
     @AuraEnabled
     public static  List<Account> getAccounts(Integer pageSize, Integer pageNumber){
         Integer offset = (pageNumber - 1) * pageSize;
         List<Account> accList = new List<Account>();
         accList = [SELECT Id, Name, AccountNumber, Industry, Phone FROM Account LIMIT :pageSize OFFSET :offset];
         return accList;
     }
      
     //Delete Account
     @AuraEnabled
     public static void deleteAccount(Account acc){
         Delete acc;
     } 
}
