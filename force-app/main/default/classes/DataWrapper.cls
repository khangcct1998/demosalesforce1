public class DataWrapper {
    
    @AuraEnabled
    public String accountName;
    @AuraEnabled
    public String registrationId;
    @AuraEnabled
    public String accountType;
    @AuraEnabled
    public String openDate;
    @AuraEnabled
    public List<String> allowedTransactions;
    @AuraEnabled
    public Address address;
    @AuraEnabled
    public List<Stocks> stocks;
    @AuraEnabled
    public Boolean accountClosedStatus;
    @AuraEnabled
    public Double totalCurrentValue;

    public class Address {
        @AuraEnabled
        public String addressLine1;
        @AuraEnabled
        public String addressLine2;
        @AuraEnabled
        public String addressLine3;
    }

    public class Stocks {
        @AuraEnabled
        public String stock;
        @AuraEnabled
        public String stockName;
        @AuraEnabled
        public String stockNumber;
        @AuraEnabled
        public String accountNumber;
        @AuraEnabled
        public String currentValue;
        @AuraEnabled
        public String totalShare;
        @AuraEnabled
        public String totalCost;
        @AuraEnabled
        public String minimumInvestment;
    }

    
    public static DataWrapper parse(String json) {
        return (DataWrapper) System.JSON.deserialize(json, DataWrapper.class);
    }
}