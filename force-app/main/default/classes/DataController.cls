public class DataController {
	@AuraEnabled
    public static DataWrapper dataFetch(){
        try{
            String json = '{\"accountName\": \"RAM REDDY\",\"registrationId\": \"501049438\",\"accountType\": \"Savings\",\"openDate\": \"08/24/2021\",\"allowedTransactions\": [\"Send\",\"Recieve\",\"Manage\"],\"address\": {\"addressLine1\": \"\",\"addressLine2\": \"\",\"addressLine3\": \"\"},\"stocks\": [{\"stock\": \"BTC\",\"stockName\": \"Bitcoin\",\"stockNumber\": \"051\",\"accountNumber\": \"12000012653\",\"currentValue\": \"161293.80\",\"totalShare\": \"5367.514\",      \"totalCost\": \"134931.56089999998\",\"minimumInvestment\": \"200.000000000000\"},{\"stock\": \"LTC\",\"stockName\": \"Litecoin\",\"stockNumber\": \"052\",\"accountNumber\": \"3338493\",\"currentValue\": \"324.80\",\"totalShare\": \"545.514\",      \"totalCost\": \"2242.56089999998\",\"minimumInvestment\": \"50.000000000000\"}],\"accountClosedStatus\": false,\"totalCurrentValue\": 170790.31}';
            DataWrapper  responseWrapper = DataWrapper.parse(json);
            return responseWrapper;
        } catch(Exception ex){
            System.debug('Error occured while fetching the documents list' + ex);
        }
        return null;
	}
}